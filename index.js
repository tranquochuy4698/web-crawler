const puppeteer = require('puppeteer');
const chalk = require('chalk');
const fs = require('fs');
require('events').EventEmitter.defaultMaxListeners = 30;

const folder = './data';
const filePath = './data/news.js';

// News page
const target = 'http://genk.vn';

const getArticles = page =>
  page.evaluate(
    async () =>
      await new Promise((resolve, reject) => {
        let titles = document.querySelectorAll('h4.knswli-title > a');
        let titlesArr = [...titles];
        const cleanedTitles = titlesArr.map(item => {
          return {
            link: 'http://genk.vn' + item.getAttribute('href')
          };
        });
        resolve(cleanedTitles);
      })
  );

const getArticlesContent = page => {
  return page.evaluate(
    async () =>
      await new Promise((resolve, reject) => {
        let newsContent = document.querySelectorAll('div#ContentDetail > p');
        let newsTitle = document.querySelector('.kbwc-title.clearfix');
        let newsLink = document.URL;
        let contentsArr = [...newsContent];
        let gatheredContents = contentsArr.map(item => ({
          title: newsTitle.innerText,
          link: newsLink,
          content: item.innerText
        }));
        resolve(gatheredContents);
      })
  );
};

const anAsyncFunction = async (item, browser) => {
  let page = await browser.newPage();
  await page.goto(item.link, { waitUntil: 'domcontentloaded' });
  return getArticlesContent(page, item);
};

(async () => {
  console.log(`Fetching data from: ${chalk.yellow(target)}`);
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  page.setViewport({ width: 1280, height: 720 });
  await page.goto(target, { waitUntil: 'domcontentloaded' });

  const articles = await getArticles(page);

  let newsContent = await Promise.all(
    articles.map(item => anAsyncFunction(item, browser))
  );

  newsContent = newsContent.map((obj, index) => {
    let mainContent = '',
      mainTitle,
      mainLink;

    obj.map(article => {
      mainTitle = article.title;
      mainLink = article.link;
      mainContent += ' ' + article.content;
    });

    return {
      title: mainTitle,
      link: mainLink,
      content: mainContent
    };
  });

  //

  if (fs.existsSync(folder) == false) {
    fs.mkdirSync(folder, { recursive: true }, err => console.log(err));
  }

  const fileContent = JSON.stringify(newsContent).replace(`[]`, ' ');

  await fs.writeFileSync(
    filePath,
    'export const data = ' + fileContent,
    err => {
      if (err) {
        console.log('Write file err: ', err.toString());
      } else {
        console.log('Write file success.');
      }
    }
  );

  await browser.close();
  console.log(`Done fetching data from: ${chalk.yellow(target)}`);
})();
