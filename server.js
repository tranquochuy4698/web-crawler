const express = require('express');
const app = express();
const chalk = require('chalk');
const port = 4000;

app.get('/', (req, res) => {
  res.set('Content-Type', 'text/html');
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`App is listening on port ${chalk.green(port)}`);
});
